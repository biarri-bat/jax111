hippo_habitat_client ^
--prune_distribution_input_design_graph "True" ^
--crossing-multiplier "10" ^
--mip_stop_gap_dist "0.20" ^
--reuse_factor 0.90 ^
--intersection-crossing-multiplier "5" ^
--dist-footprint "candidate_positions\footprint_dist.shp" ^
--mst_geometry "candidate_positions\msts.shp" ^
--fap_geometry "candidate_positions\naps.shp" ^
--fdh_geometry "candidate_positions\hubs.shp" ^
--drop-footprint "candidate_positions\footprint_drop.shp" ^
--mst-footprint "candidate_positions\footprint_mst.shp" ^
--access-footprint "candidate_positions\footprint_access.shp" ^
--candidate_graph "output_candidate_graph_editor\candidate_graph.json" ^
--graph_to_geom "output_candidate_graph_editor\graph_to_geom.json" ^
--hut_node_graph "output_preprocessor\hut_node_graph.json" ^
--demand_node_graph "output_preprocessor\demand_node_graph.json" ^
--logfile ".\output_solver\DISTFP\log.log" ^
--output_dir ".\output_solver\DISTFP" ^
--server_url "https://greenfrog.biarrinetworks.com" 


::--API_key "20e3c6ea-3a5b-11e5-9835-0cc47a44262e" ^
::--fdh-cable-blocker "cable_blocker\dist_blocker_117.shp" ^
::--server_url "http://carme.biarrinetworks.com" 
::--mst_geometry "candidate_positions\msts.shp" ^
::--fap_geometry "candidate_positions\naps.shp" ^
::--fdh_geometry "candidate_positions\hubs.shp" ^
::--drop-footprint "candidate_positions\footprint_drop.shp" ^
::--mst-footprint "candidate_positions\footprint_mst.shp" ^
::--access-footprint "candidate_positions\footprint_access.shp" ^
::--dist-footprint "candidate_positions\footprint_dist.shp" ^
