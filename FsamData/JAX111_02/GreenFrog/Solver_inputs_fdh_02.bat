hippo_habitat_client ^
--run_distribution_solver "false" ^
--mip_stop_gap_fdh "0.10" ^
--reuse_factor "0.90" ^
--crossing-multiplier "20" ^
--intersection-crossing-multiplier "5" ^
--access_cabling_algo "detailed_solve_algo" ^
--cache "output_solver\fap\design_graph.json" ^
--mst-cable-blocker "cable_blocker\mst_blocker_XXX.shp" ^
--nap-cable-blocker "cable_blocker\access_blocker_XXX.shp" ^
--candidate_graph "output_preprocessor\candidate_graph.json" ^
--graph_to_geom "output_preprocessor\graph_to_geom.json" ^
--hut_node_graph "output_preprocessor\hut_node_graph.json" ^
--demand_node_graph "output_preprocessor\demand_node_graph.json" ^
--logfile ".\output_solver\fdh\log.log" ^
--output_dir ".\output_solver\fdh" ^
--server_url "https://greenfrog.biarrinetworks.com" 

::--API_key "20e3c6ea-3a5b-11e5-9835-0cc47a44262e" ^
::--server_url "http://carme.biarrinetworks.com" 
::--mst_geometry "candidate_positions\msts.shp" ^
::--fap_geometry "candidate_positions\naps.shp" ^
::--fdh_geometry "candidate_positions\hubs.shp" ^
::--drop-footprint "candidate_positions\footprint_drop.shp" ^
::--mst-footprint "candidate_positions\footprint_mst.shp" ^
::--access-footprint "candidate_positions\footprint_access.shp" ^
::--dist-footprint "candidate_positions\footprint_dist.shp" ^
