# Layer: `GID_CAD`
### AssetID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GIDDescription
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### CAD
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### CADDescription
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GPN
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Optional
* Type: `STRING`
* Width: `5`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Quantity
* Type: `REAL`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `HUTAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### HUTID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### EngGuidelines
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ConstTypicals
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `MSTAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### MSTID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `POLE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `Paul Sulisz`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 3
* Number of features: 233
* Empty values: 0
* Random value: `JEA`

### OwnerPoleID
* Type: `STRING`
* Width: `50`
* Unique values: 229
* Number of features: 233
* Empty values: 0
* Random value: `POL-1066038`

### AlternatePoleID
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 233
* Empty values: 0
* Random value: `None`

### Use
* Type: `STRING`
* Width: `50`
* Unique values: 4
* Number of features: 233
* Empty values: 0
* Random value: ``

### Height
* Type: `REAL`
* No 'width' specified.
* Unique values: 12
* Number of features: 233
* Empty values: 0
* Random value: `40`

### Material
* Type: `STRING`
* Width: `50`
* Unique values: 4
* Number of features: 233
* Empty values: 0
* Random value: `None`

### Attachable
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `ATTACH`

### AttachmentCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 12
* Number of features: 233
* Empty values: 0
* Random value: `8`

### AttachmentHeight1
* Type: `REAL`
* No 'width' specified.
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `0`

### AttachmentHeight2
* Type: `REAL`
* No 'width' specified.
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `0`

### AttachmentHeight3
* Type: `REAL`
* No 'width' specified.
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `0`

### AttachmentSide
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### StandInstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `0`

### ClampType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### DeadendType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `Single`

### Grounding
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 233
* Empty values: 0
* Random value: `None`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 4
* Number of features: 233
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 233
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 3
* Number of features: 233
* Empty values: 0
* Random value: `None`

# Layer: `PERMIT_BUILD`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `FDHAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FDHID
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DemandSegment
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `GUY_SPAN`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `Paul Sulisz`

### ToStructure
* Type: `STRING`
* Width: `50`
* Unique values: 141
* Number of features: 142
* Empty values: 0
* Random value: `POL-1031183`

### FromStructure
* Type: `STRING`
* Width: `50`
* Unique values: 141
* Number of features: 142
* Empty values: 0
* Random value: `A1-POL-1469623`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `0`

### GuyType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `Down Guy`

### StrandSize
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `PSR`

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

# Layer: `FDHPOINT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: `Paul Sulisz`

### FDHID
* Type: `STRING`
* Width: `100`
* Unique values: 55
* Number of features: 55
* Empty values: 0
* Random value: `JAX111h-F002`

### LocationDescription
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: ``

### AddressID
* Type: `STRING`
* Width: `255`
* Unique values: 54
* Number of features: 55
* Empty values: 0
* Random value: `5291e0a736de8cb2749d0beed6f0f576`

### DFCableName
* Type: `STRING`
* Width: `150`
* Unique values: 10
* Number of features: 55
* Empty values: 0
* Random value: `JAX111h`

### AFCableName
* Type: `STRING`
* Width: `150`
* Unique values: 55
* Number of features: 55
* Empty values: 0
* Random value: `JAX111h-F006a,JAX111h-F006b,JAX111h-F006c`

### Number1x4
* Type: `INTEGER`
* Width: `150`
* Unique values: 20
* Number of features: 55
* Empty values: 0
* Random value: `6`

### Number1x8
* Type: `INTEGER`
* Width: `150`
* Unique values: 9
* Number of features: 55
* Empty values: 0
* Random value: `9`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: `PSR`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 55
* Empty values: 0
* Random value: `Biarri`

# Layer: `HUTPOINT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `Paul Sulisz`

### HUTID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `JAX111`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### LocationDescription
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### AddressID
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `53c9dc2cf7960413016cc2b5de168019`

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `PSR`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 1
* Empty values: 0
* Random value: `Biarri`

# Layer: `PERMITAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### PermitType
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### CityPermitDescription
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Agency
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ImpactedScope
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### PermitDuration
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateIdentified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateApplied
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateApproved
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateClosed
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `FAPAREA`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FAPID
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FAPType
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `CONDUIT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `Paul Sulisz`

### CalculatedLength
* Type: `REAL`
* Width: `4`
* Unique values: 28284
* Number of features: 33854
* Empty values: 0
* Random value: `133.9672`

### MeasuredLength
* Type: `REAL`
* Width: `4`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `0`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `0`

### Diameter
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 33854
* Empty values: 0
* Random value: `0.5inch`

### Category
* Type: `STRING`
* Width: `100`
* Unique values: 4
* Number of features: 33854
* Empty values: 0
* Random value: `DROP`

### ToStructure
* Type: `STRING`
* Width: `50`
* Unique values: 8473
* Number of features: 33854
* Empty values: 0
* Random value: `GLS-JAX111c-F003b,062-0001`

### FromStructure
* Type: `STRING`
* Width: `50`
* Unique values: 11775
* Number of features: 33854
* Empty values: 0
* Random value: `GLS-JAX111h-F003a,041-0002`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `PSR`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `Google`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 33854
* Empty values: 0
* Random value: `AF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `500`
* Unique values: 27327
* Number of features: 33854
* Empty values: 0
* Random value: `JAX111c-F004b,066-5,JAX111c-F004b,066-7`

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 33854
* Empty values: 0
* Random value: `Biarri`

### CableCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 15
* Number of features: 33854
* Empty values: 0
* Random value: `9`

# Layer: `SLACKLOOP`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: `Paul Sulisz`

### CableName
* Type: `STRING`
* Width: `350`
* Unique values: 213
* Number of features: 298
* Empty values: 0
* Random value: `JAX111h-F005c,123`

### CableCapacity
* Type: `STRING`
* Width: `10`
* Unique values: 4
* Number of features: 298
* Empty values: 0
* Random value: `60`

### MeasuredLength
* Type: `REAL`
* Width: `4`
* Unique values: 3
* Number of features: 298
* Empty values: 0
* Random value: `150`

### Placement
* Type: `STRING`
* Width: `100`
* Unique values: 3
* Number of features: 298
* Empty values: 0
* Random value: `Aerial`

### SegmentID
* Type: `STRING`
* Width: `90`
* Unique values: 8
* Number of features: 298
* Empty values: 0
* Random value: `6`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 3
* Number of features: 298
* Empty values: 0
* Random value: `MST`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 298
* Empty values: 0
* Random value: `Biarri`

# Layer: `BUILDINGOUTLINE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### BuildingName
* Type: `STRING`
* Width: `150`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Address
* Type: `STRING`
* Width: `150`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### City
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### State
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ZipCode
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### UnitCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### ContractStatus
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### PropertyID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### LandlordID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### FiberEquipmentID
* Type: `STRING`
* Width: `20`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `ADDRESS`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `Paul Sulisz`

### AddressID
* Type: `STRING`
* Width: `250`
* Unique values: 28477
* Number of features: 29149
* Empty values: 0
* Random value: `8c3900e0ac24c4bfde175e200286da5d`

### AddressType
* Type: `STRING`
* Width: `250`
* Unique values: 13
* Number of features: 29149
* Empty values: 0
* Random value: `CONDO`

### StreetAddress
* Type: `STRING`
* Width: `250`
* Unique values: 17082
* Number of features: 29149
* Empty values: 0
* Random value: `10797 ORCHARD WALK PL W`

### UnitNumber
* Type: `STRING`
* Width: `250`
* Unique values: 2104
* Number of features: 29149
* Empty values: 0
* Random value: `UNIT 5G`

### Zipcode
* Type: `STRING`
* Width: `250`
* Unique values: 4430
* Number of features: 29149
* Empty values: 0
* Random value: `32257-6130`

### City
* Type: `STRING`
* Width: `250`
* Unique values: 2
* Number of features: 29149
* Empty values: 0
* Random value: ``

### State
* Type: `STRING`
* Width: `250`
* Unique values: 2
* Number of features: 29149
* Empty values: 0
* Random value: ``

### Latitude
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: ``

### Longitude
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: ``

### DataSource
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `None`

### CoordinatesAccuracy
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `UNKNOWN_COORDINATE_ACCURACY`

### OnPrivateRoad
* Type: `STRING`
* Width: `5`
* Unique values: 3
* Number of features: 29149
* Empty values: 0
* Random value: `Y`

### ProjectID
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `PSR`

### DateAcceptedForDesign
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: `0`

### DropType
* Type: `STRING`
* Width: `50`
* Unique values: 4
* Number of features: 29149
* Empty values: 0
* Random value: `Aerial`

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 29149
* Empty values: 0
* Random value: ``

# Layer: `PATH`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `Paul Sulisz`

### Placement
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13350
* Empty values: 0
* Random value: `Underground`

### StrandSize
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13350
* Empty values: 0
* Random value: ``

### SquirrelGuard
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: ``

### TreeGuard
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: ``

### SurfaceType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `None`

### InstallMethod
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `None`

### Density
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `RS`

### ToneWire
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13350
* Empty values: 0
* Random value: `N`

### Easement
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13350
* Empty values: 0
* Random value: ``

### CalculatedLength
* Type: `REAL`
* Width: `4`
* Unique values: 13284
* Number of features: 13350
* Empty values: 0
* Random value: `127.8663`

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: `PSR`

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 13350
* Empty values: 0
* Random value: `AF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13350
* Empty values: 0
* Random value: ``

### ConduitDiameterSum
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 7
* Number of features: 13350
* Empty values: 0
* Random value: `8`

### CableCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 15
* Number of features: 13350
* Empty values: 0
* Random value: `8`

# Layer: `RISER`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `Paul Sulisz`

### RiserID
* Type: `STRING`
* Width: `50`
* Unique values: 48
* Number of features: 49
* Empty values: 0
* Random value: `R1-POL-1044431`

### PoleID
* Type: `STRING`
* Width: `50`
* Unique values: 48
* Number of features: 49
* Empty values: 0
* Random value: `POL-1087412`

### CableName
* Type: `STRING`
* Width: `350`
* Unique values: 47
* Number of features: 49
* Empty values: 0
* Random value: `JAX111i-F002b,061;JAX111i-F002b,067;JAX111i-F002b,063`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `None`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

### RiserCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `2`

### Diameter
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: `2`

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 49
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 49
* Empty values: 0
* Random value: ``

# Layer: `STRUCTURE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `Paul Sulisz`

### StructureName
* Type: `STRING`
* Width: `50`
* Unique values: 13111
* Number of features: 13111
* Empty values: 0
* Random value: `GLS-JAX111f-F002c,125-5-0001`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `0`

### StructureForm
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `Vault`

### PrimaryUse
* Type: `STRING`
* Width: `50`
* Unique values: 7
* Number of features: 13111
* Empty values: 0
* Random value: `Storage`

### VaultSize
* Type: `STRING`
* Width: `50`
* Unique values: 5
* Number of features: 13111
* Empty values: 0
* Random value: `LV`

### LidType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `Solid`

### Rating
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `Tier15`

### LidMaterial
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `HDPE`

### LidRing
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `N`

### Grounded
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 13111
* Empty values: 0
* Random value: `Y`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `PSR`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `Google`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 3
* Number of features: 13111
* Empty values: 0
* Random value: `MST`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 13111
* Empty values: 0
* Random value: `Biarri`

# Layer: `FIBEREQUIPMENT`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `Paul Sulisz`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `0`

### EquipmentForm
* Type: `STRING`
* Width: `25`
* Unique values: 3
* Number of features: 20048
* Empty values: 0
* Random value: `NIU`

### EquipmentName
* Type: `STRING`
* Width: `250`
* Unique values: 20048
* Number of features: 20048
* Empty values: 0
* Random value: `JAX111f-F004c,112-4_NIU`

### Placement
* Type: `STRING`
* Width: `100`
* Unique values: 3
* Number of features: 20048
* Empty values: 0
* Random value: `Aerial`

### SplitRatio
* Type: `STRING`
* Width: `25`
* Unique values: 3
* Number of features: 20048
* Empty values: 0
* Random value: `SPLT_8`

### InputPortCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `1`

### InputPortAssigned
* Type: `STRING`
* Width: `50`
* Unique values: 23
* Number of features: 20048
* Empty values: 0
* Random value: `4`

### OutputPortCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 20048
* Empty values: 0
* Random value: `2`

### OutputPortsAssigned
* Type: `STRING`
* Width: `50`
* Unique values: 591
* Number of features: 20048
* Empty values: 0
* Random value: `97,99,101,116`

### InputBlockCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `1`

### OutputBlockCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `1`

### InputStrandsAssigned
* Type: `STRING`
* Width: `250`
* Unique values: 19841
* Number of features: 20048
* Empty values: 0
* Random value: `JAX111j-F001b,061-6(1)`

### OutputStrandsAssigned
* Type: `STRING`
* Width: `250`
* Unique values: 3800
* Number of features: 20048
* Empty values: 0
* Random value: `JAX111e-F005a,027-1(1);JAX111e-F005a,027-2(1)`

### StrandsAssigned
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 20048
* Empty values: 0
* Random value: `5`

### CableCapacity
* Type: `STRING`
* Width: `10`
* Unique values: 2
* Number of features: 20048
* Empty values: 0
* Random value: `216`

### OutputPortID
* Type: `STRING`
* Width: `50`
* Unique values: 2973
* Number of features: 20048
* Empty values: 0
* Random value: `JAX111f-F003a(17)`

### AddressID
* Type: `STRING`
* Width: `250`
* Unique values: 15903
* Number of features: 20048
* Empty values: 0
* Random value: `58d76c725bb64b2c1150b14224079e8c`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 20048
* Empty values: 0
* Random value: `MST`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### CircuitID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 20048
* Empty values: 0
* Random value: `Biarri`

# Layer: `WIFI`
### WiFiID
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Latitude
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Longitude
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### Comments
* Type: `STRING`
* Width: `250`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 0
* Number of features: 0
* Empty values: 0
* This field was not found in any feature.

# Layer: `SPLICECLOSURE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: `Paul Sulisz`

### SpliceID
* Type: `STRING`
* Width: `255`
* Unique values: 719
* Number of features: 719
* Empty values: 0
* Random value: `JAX111a-F004c-AFF4`

### StructureName
* Type: `STRING`
* Width: `184`
* Unique values: 661
* Number of features: 719
* Empty values: 0
* Random value: `GLS-JAX111g-0053`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: `0`

### FiberAssignments
* Type: `STRING`
* Width: `150`
* Unique values: 695
* Number of features: 719
* Empty values: 0
* Random value: `JAX111b-F004c(109-115) + SP(116-120) + XD(1-12,37-60)`

### SpliceType
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 719
* Empty values: 0
* Random value: `SINGLE`

### Placement
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 719
* Empty values: 0
* Random value: `Underground`

### ClosureUse
* Type: `STRING`
* Width: `50`
* Unique values: 5
* Number of features: 719
* Empty values: 0
* Random value: `FAP_MCA`

### ClosureSize
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 719
* Empty values: 0
* Random value: `D`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: `PSR`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `50`
* Unique values: 2
* Number of features: 719
* Empty values: 0
* Random value: `DF`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: ``

### RibbonizedSplices
* Type: `INTEGER`
* Width: `100`
* Unique values: 2
* Number of features: 719
* Empty values: 0
* Random value: `0`

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 719
* Empty values: 0
* Random value: `Biarri`

# Layer: `ANCHOR`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `Paul Sulisz`

### AnchorID
* Type: `STRING`
* Width: `50`
* Unique values: 141
* Number of features: 142
* Empty values: 0
* Random value: `A1-POL-1087457`

### PoleID
* Type: `STRING`
* Width: `50`
* Unique values: 141
* Number of features: 142
* Empty values: 0
* Random value: `POL-1081144`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `0`

### Direction
* Type: `REAL`
* No 'width' specified.
* Unique values: 142
* Number of features: 142
* Empty values: 0
* Random value: `182.511743033`

### AnchorType
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `AC00`

### StrandSize
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `1_4`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `PSR`

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `None`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 142
* Empty values: 0
* Random value: `Biarri`

# Layer: `FIBERCABLE`
### CreationUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `Paul Sulisz`

### DateCreated
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `2016-06-30`

### DateModified
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `2016-06-30`

### LastUser
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `Paul Sulisz`

### Cablename
* Type: `STRING`
* Width: `500`
* Unique values: 20745
* Number of features: 21406
* Empty values: 0
* Random value: `JAX111b-F001b,067-3`

### Leg
* Type: `STRING`
* Width: `20`
* Unique values: 11
* Number of features: 21406
* Empty values: 0
* Random value: `d`

### Category
* Type: `STRING`
* Width: `100`
* Unique values: 4
* Number of features: 21406
* Empty values: 0
* Random value: `MST`

### BufferCount
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 5
* Number of features: 21406
* Empty values: 0
* Random value: `5`

### FibersPerBuffer
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 2
* Number of features: 21406
* Empty values: 0
* Random value: `0`

### BatchNumber_ReelNumber
* Type: `STRING`
* Width: `20`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### SerialNumber
* Type: `STRING`
* Width: `250`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### ToFeature
* Type: `STRING`
* Width: `50`
* Unique values: 19996
* Number of features: 21406
* Empty values: 0
* Random value: `JAX111h-F002b,061-2_NIU`

### FromFeature
* Type: `STRING`
* Width: `50`
* Unique values: 3691
* Number of features: 21406
* Empty values: 0
* Random value: `JAX111g-F002a-043_MST`

### CableCapacity
* Type: `STRING`
* Width: `10`
* Unique values: 5
* Number of features: 21406
* Empty values: 0
* Random value: `144`

### AssignedFiberCount
* Type: `STRING`
* Width: `250`
* Unique values: 21265
* Number of features: 21406
* Empty values: 0
* Random value: `JAX111b-F002c,104-4(1)`

### AssignedFibers
* Type: `STRING`
* Width: `50`
* Unique values: 160
* Number of features: 21406
* Empty values: 0
* Random value: `97-104`

### DeadFibers
* Type: `STRING`
* Width: `50`
* Unique values: 28
* Number of features: 21406
* Empty values: 0
* Random value: `1-12,37-60`

### SpareFibers
* Type: `STRING`
* Width: `50`
* Unique values: 159
* Number of features: 21406
* Empty values: 0
* Random value: `34-72,101-144,166-216,235-288,312-360,382-432`

### IsOddCountFront
* Type: `STRING`
* Width: `5`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `N`

### FirstFiber
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 21406
* Empty values: 0
* Random value: `37`

### Placement
* Type: `STRING`
* Width: `100`
* Unique values: 2
* Number of features: 21406
* Empty values: 0
* Random value: `Underground`

### Core
* Type: `STRING`
* Width: `112`
* Unique values: 2
* Number of features: 21406
* Empty values: 0
* Random value: `LOOSE`

### CalculatedLength
* Type: `REAL`
* Width: `4`
* Unique values: 20676
* Number of features: 21406
* Empty values: 0
* Random value: `259.0991`

### MeasuredLength
* Type: `REAL`
* Width: `4`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `0`

### StubLength
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 10
* Number of features: 21406
* Empty values: 0
* Random value: `500`

### DropLength
* Type: `INTEGER`
* No 'width' specified.
* Unique values: 8
* Number of features: 21406
* Empty values: 0
* Random value: `250`

### InstallationDate
* Type: `DATE`
* No 'width' specified.
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `0`

### SequenceIN
* Type: `STRING`
* Width: `20`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### SequenceOut
* Type: `STRING`
* Width: `20`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### SegmentID
* Type: `STRING`
* Width: `90`
* Unique values: 26
* Number of features: 21406
* Empty values: 0
* Random value: `158`

### CircuitID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### ConduitIPID
* Type: `STRING`
* Width: `350`
* Unique values: 20097
* Number of features: 21406
* Empty values: 0
* Random value: `JAX111c-F005c,JAX111c-F005b,JAX111c-F005a,005,JAX111c-F005a,004;JAX111c-F005c,JAX111c-F005b,JAX111c-F005a,003,JAX111c-F005a,005,JAX111c-F005a,004;JAX111c-F005c,JAX111c-F005b,JAX111c-F005a,003,JAX111c-F005a,005,JAX111c-F005a,001,JAX111c-F005a,004;JAX111c-F005c,JAX111c-F005b,JAX111c-F005a,005`

### GID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### P6ID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### Status
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `PSR`

### Owner
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `Google`

### Comments
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### Hierarchy
* Type: `STRING`
* Width: `24`
* Unique values: 3
* Number of features: 21406
* Empty values: 0
* Random value: `MST`

### Hyperlink
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### WorkOrderID
* Type: `STRING`
* Width: `100`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### IPID
* Type: `STRING`
* Width: `90`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### GlobalID
* Type: `STRING`
* Width: `50`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: ``

### SourceName
* Type: `STRING`
* Width: `255`
* Unique values: 1
* Number of features: 21406
* Empty values: 0
* Random value: `Biarri`

